# Rex Drone

Rex Drone is a Rex Agent running on your nodes. Rex Drone is connected via http websockets to the Rex Overlord and can send events (notifications) to it so that the overlord can trigger tasks on these special events.
Rex Drone will also speed up the execution of `rex`.

Rex Drone can be extended with custom commands.

## Commands

### inventory

The inventory command triggers an inventory run. If `rex` detects the presence of Rex Drone it will use this command to get the inventory. This will speed up the execution.

The following command will output the inventory in JSON format.
```
rex-drone inventory
```

If you need another format it can specified with the `--output` option.

```
rex-drone --output YAML inventory
```

#### Extending

The inventory can be extended with custom commands. You can write inventory plugin in any language. The ouput must be a valid YAML or JSON hash.



## Configuration

The configuration file is search in `/etc/rex/drone.conf` and `/usr/local/etc/rex/drone.conf`.

```
<Inventory>
  ScriptPath /path/to/inventory/scripts
  ScriptPath /path2/to/inventory/scripts
</Inventory>
```

