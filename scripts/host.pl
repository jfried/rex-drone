#!/usr/bin/env perl

use strict;
use warnings;

use Data::Dumper;
use Rex::Inventory;
use JSON::XS;
use XML::Simple;

$::QUIET = 1;
my %hardware = Rex::Hardware->get(qw/All/);

print encode_json(
  {
    host => {
      hostname => $hardware{Host}->{hostname},
      domain   => $hardware{Host}->{domain},
    },
  }
);
