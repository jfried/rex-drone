#!/usr/bin/env perl

use strict;
use warnings;

use Data::Dumper;
use Rex::Inventory;
use JSON::XS;
use XML::Simple;

$::QUIET = 1;
my %hardware = Rex::Hardware->get(qw/All/);

print encode_json(
  {
    system => {
      operating_system         => $hardware{Host}->{operating_system},
      operating_system_release => $hardware{Host}->{operating_system_release},
      kernel                   => $hardware{Host}->{kernelname},
    },
  }
);
