#!/usr/bin/env perl

use strict;
use warnings;

use Data::Dumper;
use Rex::Inventory;
use JSON::XS;
use XML::Simple;

$::QUIET = 1;

#my $inventory = Rex::Inventory->new;
#my $data = $inventory->get;
#print Dumper $data;

my $lshw     = qx{/usr/sbin/lshw -quiet -xml};
my %hardware = Rex::Hardware->get(qw/All/);

my $ref = XMLin( $lshw, );

my @cpus;
push @cpus,
  {
  speed  => $ref->{node}->{node}->{core}->{node}->{cpu}->{capacity}->{content},
  model  => $ref->{node}->{node}->{core}->{node}->{cpu}->{product},
  vendor => $ref->{node}->{node}->{core}->{node}->{cpu}->{vendor},
  };

my @network;
traverse_lshw(
  $ref->{node},
  "network",
  sub {
    my $data = $_[0];
    push @network,
      {
      dev           => $data->{logicalname},
      mac           => ( $data->{serial} || '00:00:00:00:00:00' ),
      model         => ( $data->{product} || 'Unknown product' ),
      vendor        => ( $data->{vendor} || 'Unknown vendor' ),
      configuration => [
        $hardware{Network}->{networkconfiguration}->{ $data->{logicalname} },
      ],
      };
  }
);

my @disks;
traverse_lshw(
  $ref->{node},
  "disk",
  sub {
    my $data = $_[0];
    push @disks,
      {
      dev    => $data->{logicalname},
      size   => $data->{size}->{content},
      model  => ( $data->{product} || 'Unknown product' ),
      vendor => ( $data->{vendor} || 'Unknown vendor' ),
      serial => ( $data->{serial} || 'Unknown serial' ),
      type   => ( $data->{id} || 'disk' ),
      };
  }
);

my @memory;
for
  my $mem ( values %{ $ref->{node}->{node}->{core}->{node}->{memory}->{node} } )
{
  push @memory,
    {
    bank  => $mem->{physid},
    model => $mem->{description},
    size  => $mem->{size}->{content},
    clock => $mem->{clock}->{content},
    };
}

print encode_json(
  {
    hardware => {
      network => \@network,
      memory  => \@memory,
      hdd     => \@disks,
      cpu     => \@cpus,
    },
  }
);

sub traverse_lshw {
  my ( $start, $class, $cb ) = @_;

  if ( ref $start eq "HASH" && exists $start->{node} ) {
    for my $key ( keys %{ $start->{node} } ) {
      traverse_lshw( $start->{node}->{$key}, $class, $cb );
    }
  }

  if ( ref $start eq "HASH" && $start->{class} && $start->{class} eq $class ) {
    $cb->($start);
  }

  if ( ref $start eq "HASH"
    && exists $start->{node}
    && $start->{node}->{class}
    && $start->{node}->{class} eq $class )
  {
    $cb->( $start->{node} );
  }

  #else {
  #print Dumper $start;
  #die;
  #}
}

