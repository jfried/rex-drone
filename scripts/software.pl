#!/usr/bin/env perl

use strict;
use warnings;

use Data::Dumper;
use Rex::Pkg;
use JSON::XS;
use XML::Simple;

$::QUIET = 1;

my $pkg      = Rex::Pkg->get;
my @packages = $pkg->get_installed();

print encode_json(
  {
    software => \@packages,
  }
);
