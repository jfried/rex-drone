use strict;
use warnings;

use Test::More tests => 6;

use_ok 'Rex::Drone';
use_ok 'Rex::Drone::Output::Interface';
use_ok 'Rex::Drone::Output::Impl::JSON';
use_ok 'Rex::Drone::Output::Impl::YAML';
use_ok 'Rex::Drone::Command';
use_ok 'Rex::Drone::Cmd::inventory';

