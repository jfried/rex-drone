#
# (c) Jan Gehring <jan.gehring@gmail.com>
#
# vim: set ts=2 sw=2 tw=0:
# vim: set expandtab:

=encoding UTF-8

=head1 NAME

Rex::Drone - Rex agent for your nodes

=head1 DESCRIPTION

Rex::Drone is an agent running on your nodes to speed up the execution of rex and to have the possibility to send events to the Rex::Overlord.

=cut

package Rex::Drone;

use Moo;
use common::sense;

use MooX::Cmd;
use MooX::Options;
use Log::Log4perl;
use Config::General;

use Data::Dumper;
use Carp;

# VERSION
our $VERSION = "0.1";

option debug => ( is => 'ro', doc => 'Switch on debug output.', default => 0 );
option output => (
  is      => 'ro',
  format  => 's',
  default => 'JSON',
  doc     => 'The output format for the inventory.'
);

my $logger;
has logger => (
  is      => 'ro',
  default => sub {
    my $self = shift;

    if ($logger) { return $logger; }
    my $debug_level = "INFO";

    if ( $self->debug ) {
      $debug_level = "DEBUG";
    }

    Log::Log4perl->init(
      {
        "log4perl.rootLogger"              => "$debug_level, LOGFILE",
        "log4perl.appender.LOGFILE"        => "Log::Log4perl::Appender::Screen",
        "log4perl.appender.LOGFILE.layout" => "PatternLayout",
        "log4perl.appender.LOGFILE.layout.ConversionPattern" => '[%d] %m%n',
      }
    );
    $logger = Log::Log4perl->get_logger('rootLogger');
    return $logger;
  },
);

my $config;
has config => (
  is      => 'ro',
  default => sub {
    my $self = shift;
    return $config if $config;

    my @config_file_locations = ( "./drone.conf", "/etc/rex/drone.conf",
      "/usr/local/etc/rex/drone.conf" );

    my ($config_file) = grep { -f $_ } @config_file_locations;

    if ( !$config_file ) {
      $self->logger->error("Configuration file not found.");
      confess "Configuration file not found.\nLocations: "
        . join( ", ", @config_file_locations );
    }

    my $conf_o = Config::General->new($config_file);
    $config = { $conf_o->getall };
    return $config;
  },
);

sub BUILD {
  my $self = shift;
  $self->logger->debug("Starting rex-drone");
}

sub execute {
  my ( $self, $args_ref, $chain_ref ) = @_;
}

sub out {
  my ( $self, $type, $data ) = @_;
  my $klass = "Rex::Drone::Output::Impl::$type";
  eval "use $klass;";

  if ($@) {
    confess "Error loading output class: $klass.\n$@";
  }

  $klass->new( app => $self )->write($data);
}

1;
