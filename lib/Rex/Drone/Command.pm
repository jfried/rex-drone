#
# (c) Jan Gehring <jan.gehring@gmail.com>
#
# vim: set ts=2 sw=2 tw=0:
# vim: set expandtab:

package Rex::Drone::Command;

use Moo;
use MooX::Cmd;
use MooX::Options;
use Data::Dumper;


sub app {
  my $self = shift;
  my ($app) = grep { ref $_ eq "Rex::Drone" } @{ $self->command_chain };
  $app ||= $self;

  return $app;
}

1;
