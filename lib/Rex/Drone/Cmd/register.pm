#
# (c) Jan Gehring <jan.gehring@gmail.com>
#
# vim: set ts=2 sw=2 tw=0:
# vim: set expandtab:

package Rex::Drone::Cmd::register;

use Moo;
use MooX::Cmd;
use MooX::Options;
use Data::Dumper;
use YAML;
use JSON::XS;
use Hash::Merge 'merge';
use File::Path 'make_path';
use Mojo::UserAgent;

extends 'Rex::Drone::Command';

option country => (
  is      => 'ro',
  default => 'XX',
  doc     => 'Country to use for the certificate request.'
);
option state => (
  is      => 'ro',
  default => 'Unknown',
  doc     => 'State to use for the certificate request.'
);
option location => (
  is      => 'ro',
  default => 'Unknown',
  doc     => 'Location (city) to use for the certificate request.'
);
option organization => (
  is      => 'ro',
  default => 'Rex Infrastructure',
  doc     => 'Organization to use for the certificate request.'
);
option unit => (
  is      => 'ro',
  default => 'Rex Drone',
  doc     => 'Organizational Unit name. For example department.'
);
option common_name => (
  is      => 'ro',
  default => '__auto__',
  doc     => 'The common name, default will be the hostname.'
);

sub execute {
  my ( $self, $args, $chain ) = @_;

  $self->app->logger->debug("Registering to Rex Overlord.");

  my $ua         = Mojo::UserAgent->new;
  my $rex_server = $self->app->config->{RexServer};

  my $c        = $self->country;
  my $state    = $self->state;
  my $location = $self->location;
  my $org      = $self->organization;
  my $unit     = $self->unit;
  my $cn       = $self->common_name;

  if ( $cn eq "__auto__" ) {
    $cn = qx{hostname -f};
    chomp $cn;
  }

  my $subj = "/C=$c/ST=$state/L=$location/O=$org/OU=$unit/CN=$cn";

  my $ssl_base = $self->app->config->{Agent}->{SSLPath};

  make_path $ssl_base;
  make_path "$ssl_base/private";
  make_path "$ssl_base/csr";
  make_path "$ssl_base/ca";
  make_path "$ssl_base/certs";

  chmod 0700, "$ssl_base/private";

  if ( !-f "$ssl_base/csr/$cn.csr" ) {
    $self->app->logger->debug("Generating certificate request.");
    my $out =
      qx{openssl req -nodes -newkey rsa:4096 -keyout $ssl_base/private/$cn.key -out $ssl_base/csr/$cn.csr -subj "$subj" 2>&1};

    if ( $? != 0 ) {
      $self->app->logger->error("Can't create ssl certificate request.");
      $self->app->logger->debug($out);
      confess "Error creating ssl certificate request. Return code: $?.";
    }

    chmod 0600, "$ssl_base/private/$cn.key";
    my $csr_content =
      eval { local ( @ARGV, $/ ) = ("$ssl_base/csr/$cn.csr"); <>; };

    my $tx =
      $ua->post( $rex_server . "/1.0/ca/csr", json => { csr => $csr_content } );
    if ( $tx->success ) {
      $self->app->logger->info(
        "Certificate request uploaded to server. Please rerun this command as soon as the certificate got signed."
      );
    }
    else {
      my $error_code = $tx->error->{code} || -1;

      $self->app->logger->error(
        "Error sending certificate request to Overlord. Error-Code: $error_code."
      );
      confess
        "Error sending certificate request to Overlord. Error-Code: $error_code.";
    }

    #$self->app->logger->info("");

    #$self->app->out( $self->app->output => $inventory );
  }
  elsif(-f "$ssl_base/csr/$cn.csr" && ! -f "$ssl_base/certs/$cn.crt") {
    $self->app->logger->info(
      "Certificate request already send. Checking if certificate got accepted..."
    );

    # first download ca file
    my $tx_ca = $ua->get("$rex_server/1.0/ca");
    if ( $tx_ca->success ) {
      my $ref = $tx_ca->res->json;
      if ( !$ref ) {
        $self->app->logger->error(
          "Error downloading ca file from rex server. Error: Can't read json response."
        );
      }
      if ( $ref->{ok} ) {
        open( my $fh_ca, ">", "$ssl_base/ca/ca.crt" )
          or confess "Error opening $ssl_base/ca/ca.crt: $!";
        print $fh_ca $ref->{data};
        close $fh_ca;

        $self->app->logger->debug(
          "Ca-File downloaded and saved in $ssl_base/ca/ca.crt.");
      }
      else {
        $self->app->logger->error(
          "Error downloading ca file from rex server. Error: "
            . $ref->{error} );
      }
    }
    else {
      my $error_code = $tx_ca->error->{code} || -1;
      $self->app->logger->error(
        "Error downloading ca file from rex server. Error-Code: $error_code.");
      exit 1;
    }

    # try to download signed certificate
    my $tx_crt = $ua->get("$rex_server/1.0/ca/cert/$cn");
    if ( $tx_crt->success ) {
      my $ref = $tx_crt->res->json;
      if ( $ref->{ok} ) {
        open( my $fh_crt, ">", "$ssl_base/certs/$cn.crt" )
          or confess "Error opening certificate file $ssl_base/certs/$cn.crt: $!";
        print $fh_crt $ref->{data};
        close $fh_crt;
      }
      else {
        $self->app->logger->error(
          "Error downloading crt file from rex server. Error: "
            . $ref->{error} );
      }
    }
    else {
      my $error_code = $tx_ca->error->{code} || -1;
      if ( $error_code == 404 ) {
        $self->app->logger->info("Certificate not signed yet.");
      }
      else {
        $self->app->logger->error(
          "Error downloading cert file. Error-Code: $error_code.");
      }
    }
  }
  elsif(-f "$ssl_base/certs/$cn.crt") {
    $self->app->logger->info("Already registered to Rex Overlord.");
  }

}

1;
