#
# (c) Jan Gehring <jan.gehring@gmail.com>
#
# vim: set ts=2 sw=2 tw=0:
# vim: set expandtab:

package Rex::Drone::Cmd::agent;

use Moo;
use MooX::Cmd;
use MooX::Options;
use Data::Dumper;
use YAML;
use JSON::XS;
use Hash::Merge 'merge';
use Net::Server::Daemonize;

use Rex::Drone::Agent;

extends 'Rex::Drone::Command';

option start     => ( is => 'ro', default => 0, doc => 'Start the daemon.' );
option daemonize => ( is => 'ro', default => 0, doc => 'Daemonize agent.' );

sub execute {
  my ( $self, $args, $chain ) = @_;

  if ( $self->start ) {
    $self->app->logger->debug("Staring drone agent.");

    if ( $self->daemonize ) {
      Net::Server::Daemonize::daemonize( 'jan', 'jan', './drone.pid' );
    }

    my $agent = Rex::Drone::Agent->new( app => $self->app );
    $agent->start;
  }

}

1;
