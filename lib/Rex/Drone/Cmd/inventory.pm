#
# (c) Jan Gehring <jan.gehring@gmail.com>
#
# vim: set ts=2 sw=2 tw=0:
# vim: set expandtab:

package Rex::Drone::Cmd::inventory;

use Moo;
use MooX::Cmd;
use MooX::Options;
use Data::Dumper;
use YAML;
use JSON::XS;
use Hash::Merge 'merge';

extends 'Rex::Drone::Command';

sub execute {
  my ( $self, $args, $chain ) = @_;

  $self->app->logger->debug("Generating inventory");

  my @inventory_scripts;
  if ( ref $self->app->config->{Inventory}->{ScriptPath} eq "ARRAY" ) {
    @inventory_scripts = @{ $self->app->config->{Inventory}->{ScriptPath} };
  }
  else {
    @inventory_scripts = ( $self->app->config->{Inventory}->{ScriptPath} );
  }

  $self->app->logger->debug(
    "Looking for inventory scripts in: " . join( ", ", @inventory_scripts ) );

  my $inventory = {};

  for my $dir (@inventory_scripts) {
    if ( -d $dir ) {
      opendir( my $dh, $dir ) or confess($!);
      while ( my $entry = readdir($dh) ) {
        next if ( $entry =~ m/^\./ );
        next unless ( -x "$dir/$entry" );

        my @in  = qx{$dir/$entry};
        my $ref = {};
        if ( $in[0] eq "---" ) {

          # yaml
          $ref = YAML::Load( join( "\n", @in ) . "\n" );
        }
        elsif ( $in[0] =~ m/^{/ ) {

          # we try json
          $ref = decode_json( join( "\n", @in ) );
        }

        $inventory = merge( $inventory, $ref );
      }
      closedir($dh);
    }
    else {
      $self->app->logger->error("$dir is not a directory.");
    }
  }

  $self->app->out( $self->app->output => $inventory );
}

1;
