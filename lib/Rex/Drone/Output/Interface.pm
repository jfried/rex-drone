#
# (c) Jan Gehring <jan.gehring@gmail.com>
#
# vim: set ts=2 sw=2 tw=0:
# vim: set expandtab:

package Rex::Drone::Output::Interface;

use Moo::Role;

requires qw(write);

has app => ( is => 'ro' );

1;
