#
# (c) Jan Gehring <jan.gehring@gmail.com>
#
# vim: set ts=2 sw=2 tw=0:
# vim: set expandtab:

package Rex::Drone::Output::Impl::Server;

use Moo;
use Mojo::UserAgent;
use Data::Dumper;

with 'Rex::Drone::Output::Interface';

sub write {
  my ( $self, $data ) = @_;

  my $ua         = Mojo::UserAgent->new;
  my $rex_server = $self->app->config->{RexServer};

  $self->app->logger->info("Sending data to rex server: $rex_server");

  my $tx = $ua->post( $rex_server, json => $data );
  if ( $tx->success ) {
    $self->app->logger->debug("Successfully send data to rex server.");
  }
  else {
    $self->app->logger->error( "Failed sending data to rex server. Error code: "
        . ( $tx->error->{code} || -1 )
        . " // Message: "
        . $tx->error->{message} );
  }
}

1;

1;
