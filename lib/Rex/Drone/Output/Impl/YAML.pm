#
# (c) Jan Gehring <jan.gehring@gmail.com>
# 
# vim: set ts=2 sw=2 tw=0:
# vim: set expandtab:
   
package Rex::Drone::Output::Impl::YAML;

use Moo;
use YAML;

with 'Rex::Drone::Output::Interface';

sub write {
  my ($self, $data) = @_;
  print YAML::Dump($data);
  print "\n";
}

1;
