#
# (c) Jan Gehring <jan.gehring@gmail.com>
# 
# vim: set ts=2 sw=2 tw=0:
# vim: set expandtab:
   
package Rex::Drone::Output::Impl::JSON;

use Moo;
use JSON::XS;

with 'Rex::Drone::Output::Interface';

sub write {
  my ($self, $data) = @_;
  my $coder = JSON::XS->new->ascii->pretty;
  print $coder->encode($data);
  print "\n";
}

1;
