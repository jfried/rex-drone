#
# (c) Jan Gehring <jan.gehring@gmail.com>
#
# vim: set ts=2 sw=2 tw=0:
# vim: set expandtab:

package Rex::Drone::Agent;

use Moo;
use Mojo::IOLoop;
use Mojo::UserAgent;
use Data::Dumper;

has app => ( is => 'ro' );

sub start {
  my ($self) = @_;

  my $ua = Mojo::UserAgent->new;
  my $ping_interval = $self->app->config->{Agent}->{PingInterval} || 5;

  $ua->websocket(
    $self->app->config->{RexServer} . '/1.0/broker' => sub {
      my ( $ua, $tx ) = @_;
      $self->app->logger->error("Websocket handshake failed.") and return
        unless $tx->is_websocket;

      $tx->on(
        json => sub {
          my ( $tx, $hash ) = @_;
          $self->app->logger->debug(
            "Got websocket message: " . Dumper($hash) );

          if ( exists $hash->{cmd} ) {
            my $cmd = join( " ", $::CMD, $hash->{cmd} );
            my $out = qx{$cmd};
            $self->app->logger->info("Executed [$cmd] with retval: [$?]");
            if ( $? != 0 ) {
              $self->app->logger->info($out);
            }
          }
        }
      );

      Mojo::IOLoop->recurring(
        $ping_interval => sub {
          $self->app->logger->debug("Sending [online] message to server.");
          $tx->send( { json => { msg => 'online' } } );
        }
      );
    }
  );

  Mojo::IOLoop->start unless Mojo::IOLoop->is_running;
}

1;
